# Patrones de Integración Empresarial

Realizado por Eduardo Aguilar R.



# Integración para el manejo de Facturación Electrónica.

## Estado

Propuesto

## Contexto

Facturación electrónica, provista por un proveedor local que dispone de una plataforma web, el proveedor ofrece su plataforma en la modalidad plataforma como servicio.

## Decisión

Se opta por utilizar un framework de integración, en este caso "Apache Camel", ya que este tiene el componente: "camel-http" el cual se puede utilizar para conectarse con la plataforma web del proveedor local de facturación electrónica.

## Consecuencias

Se debe configurar el componente camel-http para conectarse a la plataforma del proveedor de facturación electrónica.
Se debe tener en cuenta que al no saber utilizar el framework "Apache Camel" puede ser necesario contratar una consultoria. 
