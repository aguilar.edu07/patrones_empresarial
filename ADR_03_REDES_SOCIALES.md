# Patrones de Integración Empresarial

Realizado por Eduardo Aguilar R.



# Integración para el manejo de redes sociales

## Estado

Propuesto

## Contexto

La empresa necesita una solución que le permita poder publicar productos y recibir pedidos por redes sociales, ya que actualmente utilizan Facebook y WhatsApp para empresas, que le permiten presentar sus productos y contactarse con los clientes para tomar sus pedidos, ya que actualmente si la empresa ofrece un nuevo producto una persona encargada de las redes realiza la publicación manual en la página de Facebook, si un cliente se contacta por medio de WhatsApp, de igual manera si la persona que maneja el dispositivo móvil con la cuenta empresarial revisa podrá ver que alguien escribió solicitando algún producto  o información, pero los clientes escriben no solo en horario laboral por lo que se necesita dar una respuesta a estos clientes.

## Decisión

Se opta por utilizar un framework de integración, en este caso "Apache Camel", ya que este tiene los componentes: camel-facebook y camel-whatsapp los cuales nos permitirán conectar al sistema de la empresa XYZ con las redes sociales como son Facebook (Facilitará la generación de mensajes,agregar y eliminar publicaciones, likes, registros de actividad, ubicaciones, enlaces etc)  y Whatsapp (el cual envía mensajes utilizando una versión alojada en la nube de WhatsApp Business Platform)

## Consecuencias

Se debe implementar una lógica en "Apache Camel" para generar respuestas automáticas a los clientes que se contactan fuera del horario laboral.
Se debe tener en cuenta que al no saber utilizar el framework "Apache Camel" puede ser necesario contratar una consultoria. 
