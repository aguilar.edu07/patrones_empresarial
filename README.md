# PATRONES DE INTEGRACIÓN EMPRESARIAL.

##Elaborado por:
Eduardo Aguilar R

#Código versionado
https://gitlab.com/aguilar.edu07/patrones_empresarial

### Enunciado del problema presentado
* ENUNCIADO_PROBLEMA.md

### Archivos ADR
* ADR_01_FACTURACION_ELECTRONICA.md
* ADR_02_MEDIOS_PAGO.md
* ADR_03_REDES_SOCIALES.md

### Model C4
* Modelo C4-Diagrama C4-Contexto.png
* Modelo C4-Diagrama C4-Contenedor.png
* Modelo C4-Diagrama C4-Componente.png
* ModelC4-PatronesIntegracionEmpresarial.drawio