# Patrones de Integración Empresarial

Realizado por Eduardo Aguilar R.



# Integración para el manejo de medios de pago

## Estado

Propuesto

## Contexto
Medios de pago, la empresa necesita recibir pagos por medio de transferencias bancarias, además con la finalidad de reducir los tiempos de respuesta en procesar pagos y despachar las ordenes de compra, la empresa XYZ logró conseguir que la institución financiera ABC le provee el botón de pagos para recibir pagos con tarjetas de débito y crédito, el botón de pagos tiene dos métodos para que la empresa XYZ pueda utilizarlo, ofrece una API y un plugin listo para usar en Odoo.

## Decisión
Para tener las 2 formas de pago en caso que una falle se puede optar por:
* Se opta por utilizar un framework de integración, en este caso "Apache Camel", ya que este tiene el componente: "camel-rest-starter" el cual nos permite conectarnos con apis externas.
* También al tener un plugin listo para usar se puede hacer una integración a bajo nivel instalando el plugin en el odoo.

## Consecuencias
Se debe configurar el componente camel-rest-starter para consumir la API.
Se debe instalar el plugin del medio de pago, en el aplicativo Odoo.
Se debe tener en cuenta que al no saber utilizar el framework "Apache Camel" puede ser necesario contratar una consultoria. 
